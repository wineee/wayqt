/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

/**
 * STEPS to obtain the screenshot
 * 1. SCM requests to capture the output.
 * 2. We get SCF object.
 * 3. SCF emits a series of signals - one for each buffer format.
 * 4. Once all formats are received, bufferDone is emitted.
 * 5. Now, we can try to write the image.
 *    a. Search if we have a suitable format.
 *    b. Create the buffer for that format with suitable size, and stride
 *    c. If buffer was created, then perform the copy() or copy_with_damage()
 *    d. If we were successful, emit read(...) with the buffer object.
 *       Otherwise, emit ready( nullptr ) along with failed().
 * NOTE: If you called copy_with_damage(), i.e., copyWithDamage(), a series
 * of damage(...) signals will be emitted to inform the user about the regions
 * that were damaged before (and until) the copy was started. The net damage
 * is the union of all the previous damage rects.
 **/

#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <png.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <assert.h>

#include <QObject>
#include <QDebug>
#include <QImage>
#include <QGuiApplication>

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/ScreenCopy.hpp>

#include <wayland-client.h>
#include "wlr-screencopy-unstable-v1-client-protocol.h"


static wl_shm *shm = nullptr;

bool WQt::ScreenFrameBuffer::initializeBuffer( WQt::FrameBufferInfo bufInfo ) {
    bool sameInfo = true;

    sameInfo &= (info.format == bufInfo.format);
    sameInfo &= (info.width == bufInfo.width);
    sameInfo &= (info.height == bufInfo.height);
    sameInfo &= (info.stride == bufInfo.stride);

    /** Check if we have a requisite sized buffer of the correct format */
    if ( sameInfo ) {
        if ( buffer != nullptr ) {
            qDebug() << "Buffer available";
            return true;
        }
    }

    qDebug() << "Creating fresh buffer";

    int size = info.stride * info.height;

    char shm_name[] = "/tmp/wf-recorder-shared-XXXXXX";
    int  fd         = mkstemp( shm_name );

    int ret;
    while ( (ret = ftruncate( fd, size ) ) == EINTR ) {
        // No-op
    }

    if ( ret < 0 ) {
        close( fd );

        /** Failed to create backing file */
        return false;
    }

    unlink( shm_name );

    data = mmap( NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0 );

    if ( data == MAP_FAILED ) {
        qCritical() << "mmap() failed";
        close( fd );

        return false;
    }

    WQt::Registry *reg = new WQt::Registry( WQt::Wayland::display() );

    reg->setup();

    shm = reg->waylandShm();

    if ( shm == nullptr ) {
        qCritical() << "Unable to allocate shared memory";
        return false;
    }

    struct wl_shm_pool *pool = wl_shm_create_pool( shm, fd, size );

    close( fd );

    buffer = wl_shm_pool_create_buffer( pool, 0, info.width, info.height, info.stride, info.format );

    wl_shm_pool_destroy( pool );

    return true;
}


WQt::ScreenCopyManager::ScreenCopyManager( zwlr_screencopy_manager_v1 *mgr ) {
    mObj = mgr;
}


WQt::ScreenCopyManager::~ScreenCopyManager() {
    zwlr_screencopy_manager_v1_destroy( mObj );
}


WQt::ScreenCopyFrame *WQt::ScreenCopyManager::captureOutput( bool drawCursor, QScreen *screen ) {
    wl_output *output = WQt::Utils::wlOutputFromQScreen( screen );
    struct zwlr_screencopy_frame_v1 *frame = zwlr_screencopy_manager_v1_capture_output( mObj, (drawCursor ? 1 : 0), output );

    return new ScreenCopyFrame( frame );
}


WQt::ScreenCopyFrame *WQt::ScreenCopyManager::captureOutputRegion( bool drawCursor, QScreen *screen, QRect rect ) {
    wl_output *output = WQt::Utils::wlOutputFromQScreen( screen );
    struct zwlr_screencopy_frame_v1 *frame = zwlr_screencopy_manager_v1_capture_output_region(
        mObj, (drawCursor ? 1 : 0), output, rect.x(), rect.y(), rect.width(), rect.height()
    );

    return new ScreenCopyFrame( frame );
}


zwlr_screencopy_manager_v1 *WQt::ScreenCopyManager::get() {
    return mObj;
}


WQt::ScreenCopyFrame::ScreenCopyFrame( zwlr_screencopy_frame_v1 *frame ) {
    mObj = frame;

    if ( wl_proxy_get_listener( (wl_proxy *)mObj ) != &mListener ) {
        zwlr_screencopy_frame_v1_add_listener( mObj, &mListener, this );
    }
}


WQt::ScreenCopyFrame::~ScreenCopyFrame() {
    zwlr_screencopy_frame_v1_destroy( mObj );
}


void WQt::ScreenCopyFrame::setup() {
    if ( mIsSetup == false ) {
        mIsSetup = true;

        if ( mBufferDonePending ) {
            mBufferDonePending = false;
            emit bufferDone();
        }
    }
}


QList<WQt::FrameBufferInfo> WQt::ScreenCopyFrame::availableFormats() {
    return mReceivedBuffers;
}


void WQt::ScreenCopyFrame::attachBuffer( WQt::ScreenFrameBuffer *buf ) {
    mBuffer = buf;
}


void WQt::ScreenCopyFrame::copy() {
    if ( mBuffer == nullptr ) {
        qWarning() << "No buffer attached. Call attachBuffer(...) before calling this function.";
        return;
    }

    if ( mBuffer->buffer == nullptr ) {
        qWarning() << "Failed to create buffer with format" << mBuffer->info.format;
        return;
    }

    zwlr_screencopy_frame_v1_copy( mObj, mBuffer->buffer );
}


void WQt::ScreenCopyFrame::copyWithDamage() {
    if ( mBuffer->buffer == nullptr ) {
        qWarning() << "Failed to create buffer with format" << mBuffer->info.format;
        return;
    }

    zwlr_screencopy_frame_v1_copy_with_damage( mObj, mBuffer->buffer );
}


zwlr_screencopy_frame_v1 *WQt::ScreenCopyFrame::get() {
    return mObj;
}


void WQt::ScreenCopyFrame::handleBuffer( void *data, struct zwlr_screencopy_frame_v1 *, uint32_t fmt, uint32_t w, uint32_t h, uint32_t stride ) {
    ScreenCopyFrame *scrnFrame = reinterpret_cast<ScreenCopyFrame *>(data);
    FrameBufferInfo info       = { (wl_shm_format)fmt, w, h, stride };

    scrnFrame->mReceivedBuffers << info;
}


void WQt::ScreenCopyFrame::handleFlags( void *data, struct zwlr_screencopy_frame_v1 *, uint32_t flags ) {
    ScreenCopyFrame *scrnFrame = reinterpret_cast<ScreenCopyFrame *>(data);

    scrnFrame->mYInvert = flags & ZWLR_SCREENCOPY_FRAME_V1_FLAGS_Y_INVERT;
}


void WQt::ScreenCopyFrame::handleReady( void *data, struct zwlr_screencopy_frame_v1 *, uint32_t secs_hi, uint32_t secs_lo, uint32_t nsecs ) {
    ScreenCopyFrame *scrnFrame = reinterpret_cast<ScreenCopyFrame *>(data);

    scrnFrame->mBuffer->time.secs  = ( (1ll * secs_hi) << 32ll) | secs_lo;
    scrnFrame->mBuffer->time.nsecs = nsecs;

    emit scrnFrame->ready( scrnFrame->mBuffer );

    zwlr_screencopy_frame_v1_destroy( scrnFrame->mObj );
    scrnFrame->mObj = nullptr;
}


void WQt::ScreenCopyFrame::handleFailed( void *data, struct zwlr_screencopy_frame_v1 * ) {
    ScreenCopyFrame *scrnFrame = reinterpret_cast<ScreenCopyFrame *>(data);
    emit scrnFrame->failed();
    emit scrnFrame->ready( nullptr );
}


void WQt::ScreenCopyFrame::handleDamage( void *data, struct zwlr_screencopy_frame_v1 *, uint32_t x, uint32_t y, uint32_t w, uint32_t h ) {
    ScreenCopyFrame *scrnFrame = reinterpret_cast<ScreenCopyFrame *>(data);

    emit scrnFrame->damage( QRect( x, y, w, h ) );
}


void WQt::ScreenCopyFrame::handleLinuxDmabuf( void *, struct zwlr_screencopy_frame_v1 *, uint32_t, uint32_t, uint32_t ) {
    /** YET to be implemented */
}


void WQt::ScreenCopyFrame::handleBufferDone( void *data, struct zwlr_screencopy_frame_v1 * ) {
    ScreenCopyFrame *scrnFrame = reinterpret_cast<ScreenCopyFrame *>(data);

    if ( scrnFrame->mIsSetup ) {
        emit scrnFrame->bufferDone();
    }

    else {
        scrnFrame->mBufferDonePending = true;
    }
}


const zwlr_screencopy_frame_v1_listener WQt::ScreenCopyFrame::mListener = {
    handleBuffer,
    handleFlags,
    handleReady,
    handleFailed,
    handleDamage,
    handleLinuxDmabuf,
    handleBufferDone,
};
