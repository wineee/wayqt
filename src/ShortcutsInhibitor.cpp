/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <QDebug>
#include <QWindow>

#include <wayland-client.h>

#include "wayqt/ShortcutsInhibitor.hpp"
#include "wayqt/WayQtUtils.hpp"

#include "keyboard-shortcuts-inhibit-unstable-v1-client-protocol.h"

/**
 * WQt::ShortcutsInhibitManager
 */

WQt::ShortcutsInhibitManager::ShortcutsInhibitManager( zwp_keyboard_shortcuts_inhibit_manager_v1 *iimgr ) : QObject() {
    mObj = iimgr;
}


WQt::ShortcutsInhibitManager::~ShortcutsInhibitManager() {
    zwp_keyboard_shortcuts_inhibit_manager_v1_destroy( mObj );
}


WQt::ShortcutsInhibitor *WQt::ShortcutsInhibitManager::inhibitShortcuts( QWindow *win, wl_seat *seat ) {
    if ( mObj == nullptr ) {
        return nullptr;
    }

    if ( win == nullptr ) {
        return nullptr;
    }

    wl_surface *surf = WQt::Utils::wlSurfaceFromQWindow( win );

    return new WQt::ShortcutsInhibitor( zwp_keyboard_shortcuts_inhibit_manager_v1_inhibit_shortcuts( mObj, surf, seat ) );
}


zwp_keyboard_shortcuts_inhibit_manager_v1 *WQt::ShortcutsInhibitManager::get() {
    return mObj;
}


/**
 * WQt::ShortcutsInhibitor
 */

WQt::ShortcutsInhibitor::ShortcutsInhibitor( zwp_keyboard_shortcuts_inhibitor_v1 *ii ) : QObject() {
    mObj = ii;
    zwp_keyboard_shortcuts_inhibitor_v1_add_listener( mObj, &mListener, this );
}


WQt::ShortcutsInhibitor::~ShortcutsInhibitor() {
    zwp_keyboard_shortcuts_inhibitor_v1_destroy( mObj );
}


bool WQt::ShortcutsInhibitor::isActive() {
    return mIsActive;
}


zwp_keyboard_shortcuts_inhibitor_v1 *WQt::ShortcutsInhibitor::get() {
    return mObj;
}


void WQt::ShortcutsInhibitor::handleActive( void *data, zwp_keyboard_shortcuts_inhibitor_v1 * ) {
    WQt::ShortcutsInhibitor *inhibitor = reinterpret_cast<WQt::ShortcutsInhibitor *>(data);

    inhibitor->mIsActive = true;
    emit inhibitor->active();
}


void WQt::ShortcutsInhibitor::handleInactive( void *data, zwp_keyboard_shortcuts_inhibitor_v1 * ) {
    WQt::ShortcutsInhibitor *inhibitor = reinterpret_cast<WQt::ShortcutsInhibitor *>(data);

    inhibitor->mIsActive = false;
    emit inhibitor->inactive();
}


const struct zwp_keyboard_shortcuts_inhibitor_v1_listener WQt::ShortcutsInhibitor::mListener = {
    handleActive,
    handleInactive
};
