/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <signal.h>
#include <wayqt/WayfireViewKill.hpp>

#include "wayfire-kill-view-client-protocol.h"

WQt::WayfireViewKill::WayfireViewKill( struct wf_kill_view_base *infoBase ) {
    mObj = infoBase;

    wf_kill_view_base_add_listener( mObj, &mListener, this );
}


WQt::WayfireViewKill::~WayfireViewKill() {
    wf_kill_view_base_destroy( mObj );
}


void WQt::WayfireViewKill::kill( int sigNum ) {
    if ( sigNum > 0 ) {
        connect(
            this, &WQt::WayfireViewKill::pid, [ = ] ( pid_t view_pid ) {
                ::kill( view_pid, sigNum );
            }
        );
    }

    wf_kill_view_base_view_kill( mObj );
}


void WQt::WayfireViewKill::handleViewPid( void *data, struct wf_kill_view_base *, int32_t view_pid ) {
    WayfireViewKill *wfKill = reinterpret_cast<WayfireViewKill *>(data);

    emit wfKill->pid( view_pid );
}


void WQt::WayfireViewKill::handleCancel( void *data, struct wf_kill_view_base * ) {
    WayfireViewKill *wfKill = reinterpret_cast<WayfireViewKill *>(data);

    emit wfKill->canceled();
}


const struct wf_kill_view_base_listener WQt::WayfireViewKill::mListener = {
    .view_pid = handleViewPid,
    .cancel   = handleCancel,
};
