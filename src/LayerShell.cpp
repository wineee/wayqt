/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <QDebug>
#include <QWindow>

#include <wayland-client.h>

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/LayerShell.hpp>
#include <wayqt/XdgPopup.hpp>

/** PRIVATE QT HEADERS: THESE CAN CHANGE AT ANY TIME!! */
#include <private/qwaylanddisplay_p.h>
#include <private/qwaylandshellintegrationfactory_p.h>
#include <private/qwaylandwindow_p.h>

#include "wlr-layer-shell-unstable-v1-client-protocol.h"
#include "xdg-shell-client-protocol.h"

#include "LayerShellImpl.hpp"

using namespace WQt;

/**
 * LayerShell
 */

LayerShell::LayerShell( zwlr_layer_shell_v1 *lShell, uint version ) {
    /* This is fully developed in wl_registry's handle_global */
    mObj     = lShell;
    mVersion = version;
}


LayerShell::~LayerShell() {
    if ( mObj != nullptr ) {
        zwlr_layer_shell_v1_destroy( mObj );
    }
}


LayerSurface *LayerShell::forWindow( QWindow *window ) {
    QtWaylandClient::QWaylandWindow *waylandWindow = dynamic_cast<QtWaylandClient::QWaylandWindow *>(window->handle() );

    if ( waylandWindow == nullptr ) {
        return nullptr;
    }

    waylandWindow->setVisible( true );

    LayerSurfaceImpl *surface = qobject_cast<LayerSurfaceImpl *>( waylandWindow->shellSurface() );

    if ( surface == nullptr ) {
        return nullptr;
    }

    QObject::connect(
        surface, &LayerSurfaceImpl::configureSize, [ window ] ( QSize size ) {
            window->resize( size );
        }
    );

    QObject::connect(
        surface, &LayerSurfaceImpl::closeWindow, [ window ] () {
            window->close();
        }
    );

    return new LayerSurface( surface );
}


LayerSurface *LayerShell::getLayerSurface( QWindow *window, wl_output *output, LayerShell::LayerType layer, const QString& lyrNs ) {
    if ( mObj == nullptr ) {
        return nullptr;
    }

    wl_surface *surface = WQt::Utils::wlSurfaceFromQWindow( window );

    /* We need a valid wl_surface */
    if ( surface == nullptr ) {
        return nullptr;
    }

    /* If the output is a nullptr, we will use Qt's primary screen */
    if ( output == nullptr ) {
        output = WQt::Utils::wlOutputFromQScreen( window->screen() );
    }

    zwlr_layer_surface_v1 *lyr_surf = zwlr_layer_shell_v1_get_layer_surface( mObj, surface, output, layer, lyrNs.toUtf8().constData() );

    LayerSurface *lyrSurf = new LayerSurface( window, lyr_surf, mVersion );

    return lyrSurf;
}


zwlr_layer_shell_v1 *LayerShell::get() {
    return mObj;
}


/**
 * LayerSurface
 */

LayerSurface::LayerSurface( QWindow *window, zwlr_layer_surface_v1 *surf, uint version ) {
    QtWaylandClient::QWaylandWindow *waylandWindow = dynamic_cast<QtWaylandClient::QWaylandWindow *>(window->handle() );

    if ( waylandWindow != nullptr ) {
        /** make it visible, if it's not already visible */
        waylandWindow->setVisible( true );

        impl = new LayerSurfaceImpl( waylandWindow, surf, version );

        connect(
            impl, &LayerSurfaceImpl::configureSize, [ window ] ( QSize size ) {
                window->resize( size );
            }
        );

        connect(
            impl, &LayerSurfaceImpl::closeWindow, [ window ] () {
                window->close();
            }
        );
    }
}


LayerSurface::LayerSurface( LayerSurfaceImpl *impl ) {
    this->impl = impl;
}


LayerSurface::~LayerSurface() {
    delete impl;
    impl = nullptr;
}


bool LayerSurface::isValid() {
    return (impl != nullptr);
}


void LayerSurface::apply() {
    if ( impl == nullptr ) {
        return;
    }

    impl->apply();
}


void LayerSurface::setSurfaceSize( const QSize& surfaceSize ) {
    if ( impl == nullptr ) {
        return;
    }

    impl->setSurfaceSize( surfaceSize );
}


void LayerSurface::setAnchors( const SurfaceAnchors& anchors ) {
    if ( impl == nullptr ) {
        return;
    }

    impl->setAnchors( anchors );
}


void LayerSurface::setExclusiveZone( int exclusiveZone ) {
    if ( impl == nullptr ) {
        return;
    }

    impl->setExclusiveZone( exclusiveZone );
}


void LayerSurface::setMargins( const QMargins& margins ) {
    if ( impl == nullptr ) {
        return;
    }

    impl->setMargins( margins );
}


void LayerSurface::setKeyboardInteractivity( LayerSurface::FocusType focusType ) {
    if ( impl == nullptr ) {
        return;
    }

    impl->setKeyboardInteractivity( focusType );
}


void LayerSurface::setLayer( WQt::LayerShell::LayerType type ) {
    if ( impl == nullptr ) {
        return;
    }

    impl->setLayer( type );
}


void LayerSurface::getPopup( std::any surf ) {
    if ( impl == nullptr ) {
        return;
    }

    struct ::xdg_popup *popup = std::any_cast<struct ::xdg_popup *>( surf );

    if ( popup != nullptr ) {
        impl->getPopup( popup );
    }
}


zwlr_layer_surface_v1 *LayerSurface::get() {
    if ( impl == nullptr ) {
        return nullptr;
    }

    return impl->get();
}


QtWaylandClient::QWaylandShellSurface *LayerSurface::getShellSurface() {
    if ( impl == nullptr ) {
        return nullptr;
    }

    return (QtWaylandClient::QWaylandShellSurface *)impl;
}
