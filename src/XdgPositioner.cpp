/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <QtCore>

#include <qpa/qplatformnativeinterface.h>
#include <wayland-client.h>
#include "xdg-shell-client-protocol.h"

#include "wayqt/XdgPositioner.hpp"

WQt::XdgPositioner::XdgPositioner() {
    mObj         = nullptr;
    mInitialSize = QSize();
    mAnchorRect  = QRect();
}


WQt::XdgPositioner::XdgPositioner( const WQt::XdgPositioner *other ) {
    mObj         = other->mObj;
    mInitialSize = other->mInitialSize;
    mAnchorRect  = other->mAnchorRect;
}


WQt::XdgPositioner::XdgPositioner( xdg_positioner *pos, const QSize& initialSize, const QRect& anchor ) {
    mObj         = pos;
    mInitialSize = initialSize;
    mAnchorRect  = anchor;
}


WQt::XdgPositioner::~XdgPositioner() {
    xdg_positioner_destroy( mObj );
    mObj = nullptr;
}


WQt::XdgPositioner::Anchors WQt::XdgPositioner::anchorEdge() const {
    return mAnchorEdge;
}


void WQt::XdgPositioner::setAnchorEdge( Anchors edge ) {
    mAnchorEdge = edge;
    xdg_positioner_set_anchor( mObj, edge );
}


WQt::XdgPositioner::Gravity WQt::XdgPositioner::gravity() const {
    return mGravity;
}


void WQt::XdgPositioner::setGravity( Gravity edge ) {
    mGravity = edge;
    xdg_positioner_set_gravity( mObj, edge );
}


QRect WQt::XdgPositioner::anchorRect() const {
    return mAnchorRect;
}


void WQt::XdgPositioner::setAnchorRect( const QRect& rect ) {
    mAnchorRect = rect;
    xdg_positioner_set_anchor_rect( mObj, rect.x(), rect.y(), rect.width(), rect.height() );
}


QSize WQt::XdgPositioner::initialSize() const {
    return mInitialSize;
}


void WQt::XdgPositioner::setInitialSize( const QSize& size ) {
    mInitialSize = size;
    xdg_positioner_set_size( mObj, size.width(), size.height() );
}


WQt::XdgPositioner::Constraints WQt::XdgPositioner::constraints() const {
    return mConstraints;
}


void WQt::XdgPositioner::setConstraints( Constraints constraints ) {
    mConstraints = constraints;
    xdg_positioner_set_constraint_adjustment( mObj, constraints );
}


QPoint WQt::XdgPositioner::anchorOffset() const {
    return mAnchorOffset;
}


void WQt::XdgPositioner::setAnchorOffset( const QPoint& offset ) {
    mAnchorOffset = offset;
    xdg_positioner_set_offset( mObj, offset.x(), offset.y() );
}


void WQt::XdgPositioner::setReactive() {
    xdg_positioner_set_reactive( mObj );
}


xdg_positioner *WQt::XdgPositioner::get() {
    return mObj;
}
