/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#include <iostream>
#include <signal.h>

#include <QFont>
#include <QDebug>
#include <QLabel>
#include <QProcess>
#include <QApplication>
#include <QFontDatabase>

#include <wayqt/Registry.hpp>
#include <wayqt/WayQtUtils.hpp>
#include <wayqt/WayfireViewKill.hpp>

int main( int argc, char *argv[] ) {
    QApplication *app = new QApplication( argc, argv );

    WQt::Registry *reg = new WQt::Registry( WQt::Wayland::display() );

    reg->setup();

    WQt::WayfireViewKill *wfk = nullptr;

    if ( reg->waitForInterface( WQt::Registry::WayfireViewKillInterface ) ) {
        wfk = reg->wayfireKill();
    }

    else {
        qFatal( "Wayfire information protocol not advertised by compositor. Is wf-info plugin enabled?" );
    }

    QObject::connect(
        wfk, &WQt::WayfireViewKill::pid, [ = ] ( int pid ) {
            qDebug() << "View with pid" << pid << "killed.";
            app->quit();
        }
    );

    QObject::connect(
        wfk, &WQt::WayfireViewKill::canceled, [ = ] () {
            qDebug() << "Kill operation was canceled";
            app->quit();
        }
    );

    wfk->kill();

    return app->exec();
}
