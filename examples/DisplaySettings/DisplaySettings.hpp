/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once


#include <QtCore>
#include <QtWidgets>

#include <wayland-client.h>
#include <wayqt/Registry.hpp>
#include <wayqt/OutputManager.hpp>

namespace WQt {
    class OutputManager;
}

class DisplaySettings : public QMainWindow {
    Q_OBJECT;

    public:
        enum OutputTransform {
            /**
             * no transform
             */
            WL_OUTPUT_TRANSFORM_NORMAL      = 0,

            /**
             * 90 degrees counter-clockwise
             */
            WL_OUTPUT_TRANSFORM_90          = 1,

            /**
             * 180 degrees counter-clockwise
             */
            WL_OUTPUT_TRANSFORM_180         = 2,

            /**
             * 270 degrees counter-clockwise
             */
            WL_OUTPUT_TRANSFORM_270         = 3,

            /**
             * 180 degree flip around a vertical axis
             */
            WL_OUTPUT_TRANSFORM_FLIPPED     = 4,

            /**
             * flip and rotate 90 degrees counter-clockwise
             */
            WL_OUTPUT_TRANSFORM_FLIPPED_90  = 5,

            /**
             * flip and rotate 180 degrees counter-clockwise
             */
            WL_OUTPUT_TRANSFORM_FLIPPED_180 = 6,

            /**
             * flip and rotate 270 degrees counter-clockwise
             */
            WL_OUTPUT_TRANSFORM_FLIPPED_270 = 7,
        };

        DisplaySettings();

    private:
        QComboBox *outputs;
        QComboBox *modes;
        QComboBox *rotations;
        QCheckBox *flipped;
        QCheckBox *enabled;
        QSpinBox *scale;
        QSpinBox *posX, *posY;

        WQt::OutputManager *opMgr;

        void createGUI();
};
