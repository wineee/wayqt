project(
	'Qt LayerShell Integration',
	'c',
	'cpp',
	version: '0.2.0',
	license: 'MIT',
	meson_version: '>=0.59.0',
	default_options: [
		'cpp_std=c++17',
		'c_std=c11',
		'warning_level=2',
		'werror=false',
	],
)

add_project_arguments( '-fPIC', language: ['cpp', 'c'])

Qt = import( 'qt6' )

QtDeps = dependency(
	'qt6',
	modules: ['Core', 'Gui', 'WaylandClient'],
	private_headers: [ 'Core', 'Gui', 'WaylandClient', 'WaylandGlobalPrivate' ],
	required : true
)

qmake = find_program( [ 'qmake-qt6', 'qmake6' ], required: true )
ret = run_command( qmake, '-query', 'QT_INSTALL_HEADERS', check: true )
QtHeaderPath = ret.stdout().strip()

QtGlobal = [
	'@0@/QtWaylandGlobal/@1@'.format( QtHeaderPath, QtDeps.version() ),
	'@0@/QtWaylandGlobal/@1@/QtWaylandGlobal'.format( QtHeaderPath, QtDeps.version() )
]

wayqt = dependency(
	'wayqt-qt6',
	required : true
)

wayland_client = dependency( 'wayland-client' )

subdir( 'protos' )

Moc = Qt.compile_moc(
	headers: [ 'LayerShellIntegration.hpp' ],
	sources: [ 'QtLayerShellIntegrationPlugin.cpp' ],
	dependencies: [QtDeps],
	include_directories: QtGlobal,
)

shared_library(
	'desq-layershell-integration',
	[ 'LayerShellIntegration.cpp', 'QtLayerShellIntegrationPlugin.cpp', Moc ],
	install : false,
	dependencies : [QtDeps, wayqt, protos],
	include_directories: QtGlobal,
)
