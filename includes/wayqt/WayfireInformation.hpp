/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

#include <QSize>
#include <QPoint>
#include <QObject>

struct wf_info_base;
struct wf_info_base_listener;

namespace WQt {
    class WayfireInformation;

    typedef struct wayfire_view_info_t {
        uint32_t uuid;
        QString  output;
        uint32_t outputId;
        uint32_t pid;
        QPoint   workspace;
        QString  appId;
        QString  title;
        QString  role;
        QPoint   position;
        QSize    size;
        bool     isXwayland;
        bool     hasFocus;
    }
    WayfireViewInfo;
}

class WQt::WayfireInformation : public QObject {
    Q_OBJECT;

    public:
        WayfireInformation( wf_info_base *infoBase );
        ~WayfireInformation();

        void getViewInfo();
        void getViewInfoFromUuid( uint32_t uuid );
        void getAllViewInfo();

        wf_info_base *get();

    private:
        static void handleViewInfo( void *data,            /** this object */
                                    struct wf_info_base *, /** pointer to underlying  */
                                    uint32_t,              /** view uuid */
                                    int32_t,               /** process id of the view */
                                    int32_t,               /** workspace x */
                                    int32_t,               /** workspace y */
                                    const char *,          /** appId */
                                    const char *,          /** title */
                                    const char *,          /** role (like DE, etc) */
                                    int32_t,               /** position x */
                                    int32_t,               /** position y */
                                    int32_t,               /** width */
                                    int32_t,               /** height */
                                    int32_t,               /** is xwayland? */
                                    int32_t,               /** is focused? */
                                    const char *,          /** view's output name */
                                    uint32_t               /** view's output id */
        );

        static void handleDone( void *, struct wf_info_base * );

        /** Raw C pointer to this class */
        wf_info_base *mObj;

        /** Listener */
        static const wf_info_base_listener mListener;

    Q_SIGNALS:
        /** Emit the information of the view */
        void viewInfo( WQt::WayfireViewInfo );

        /** Info about all the views has been sent */
        void done();
};

Q_DECLARE_METATYPE( WQt::WayfireViewInfo );
