/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

#include <QObject>
#include <QSize>

struct wl_surface;
struct wl_output;

struct ext_session_lock_manager_v1;
struct ext_session_lock_v1;
struct ext_session_lock_surface_v1;
struct ext_session_lock_v1_listener;
struct ext_session_lock_surface_v1_listener;

class QWindow;

namespace WQt {
    class SessionLockManager;
    class SessionLock;
    class SessionLockSurface;
}

class WQt::SessionLockManager : public QObject {
    Q_OBJECT;

    public:
        SessionLockManager( ext_session_lock_manager_v1 *lockMgr );
        ~SessionLockManager();

        WQt::SessionLock *lock();

        ext_session_lock_manager_v1 *get();

    private:
        /** Raw C pointer to this class */
        ext_session_lock_manager_v1 *mObj;
};

class WQt::SessionLock : public QObject {
    Q_OBJECT;

    public:
        enum Error {
            InvalidDestroy = 0,         // Attempted to destroy session lock while locked.
            InvalidUnlock,              // Unlock requested but locked event was never sent.
            RoleExists,                 // Given wl_surface already has a role.
            DuplicateOutput,            // Given output already has a lock surface.
            AlreadyConstructed,         // Given wl_surface has a buffer attached or committed.
        };

        SessionLock( ext_session_lock_v1 *sessLock );
        ~SessionLock();

        /** Setup the listener. First connect the signals to your slots, and then call this */
        void setup();

        /** Check if the screen is locked with this instance */
        bool isLocked();

        WQt::SessionLockSurface * getLockSurface( QWindow *window, wl_output *output );
        void unlockAndDestroy();

        ext_session_lock_v1 *get();

    private:
        static void handleFinished( void *, ext_session_lock_v1 * );
        static void handleLocked( void *, ext_session_lock_v1 * );

        /** Raw C pointer to this class */
        ext_session_lock_v1 *mObj;

        /** Flag for locked state */
        bool mLocked = false;

        /** Listener */
        static const ext_session_lock_v1_listener mListener;

    Q_SIGNALS:
        void lockAcquired();
        void lockDestroyed();
        void lockFailed();
};

class WQt::SessionLockSurface : public QObject {
    Q_OBJECT;

    public:
        enum Error {
            CommitBeforeFirstAck = 0,         // Surface committed before first ack_configure request
            NullBuffer,                       // Surface committed with a null buffer
            DimensionsMismatch,               // Failed to match ack'd width/height
            InvalidSerial,                    // Serial provided in ack_configure is invalid
        };

        SessionLockSurface( ext_session_lock_surface_v1 *lockSurf );
        ~SessionLockSurface();

        /** Setup the listener. First connect the signals to your slots, and then call this */
        void setup();

        ext_session_lock_surface_v1 *get();

    private:
        static void handleConfigure( void *, ext_session_lock_surface_v1 *, uint32_t serial, uint32_t width, uint32_t height );

        /** Raw C pointer to this class */
        ext_session_lock_surface_v1 *mObj;

        /** Listener */
        static const ext_session_lock_surface_v1_listener mListener;

        bool mIsSetup = false;
        QSize pendingResize;

    Q_SIGNALS:
        void resizeLockSurface( QSize );
};
