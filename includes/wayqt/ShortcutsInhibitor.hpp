/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

#include <QObject>
#include <QWindow>
#include <QSize>

struct wl_seat;
struct zwp_keyboard_shortcuts_inhibit_manager_v1;
struct zwp_keyboard_shortcuts_inhibitor_v1;
struct zwp_keyboard_shortcuts_inhibitor_v1_listener;

namespace WQt {
    class ShortcutsInhibitManager;
    class ShortcutsInhibitor;
}

class WQt::ShortcutsInhibitManager : public QObject {
    Q_OBJECT;

    public:
        ShortcutsInhibitManager( zwp_keyboard_shortcuts_inhibit_manager_v1 *iimgr );
        ~ShortcutsInhibitManager();

        WQt::ShortcutsInhibitor *inhibitShortcuts( QWindow *, wl_seat * );

        zwp_keyboard_shortcuts_inhibit_manager_v1 *get();

    private:
        zwp_keyboard_shortcuts_inhibit_manager_v1 *mObj;
};

class WQt::ShortcutsInhibitor : public QObject {
    Q_OBJECT;

    public:
        ShortcutsInhibitor( zwp_keyboard_shortcuts_inhibitor_v1 *inhibitor );
        ~ShortcutsInhibitor();

        bool isActive();

        zwp_keyboard_shortcuts_inhibitor_v1 *get();

    private:
        static void handleActive( void *, zwp_keyboard_shortcuts_inhibitor_v1 * );
        static void handleInactive( void *, zwp_keyboard_shortcuts_inhibitor_v1 * );

        zwp_keyboard_shortcuts_inhibitor_v1 *mObj;
        static const zwp_keyboard_shortcuts_inhibitor_v1_listener mListener;

        bool mIsActive = false;

    Q_SIGNALS:
        void active();
        void inactive();
};
