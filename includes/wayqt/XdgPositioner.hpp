/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

#include <QPoint>
#include <QSize>
#include <QRect>

struct xdg_positioner;

namespace WQt {
    class XdgPositioner;
}

class WQt::XdgPositioner {
    public:

        /*
         * Flags describing how a popup should be reposition if constrained
         */
        enum class Constraint {
            None    = 0,
            SlideX  = 1 << 0,
            SlideY  = 1 << 1,
            FlipX   = 1 << 2,
            FlipY   = 1 << 3,
            ResizeX = 1 << 4,
            ResizeY = 1 << 5,
        };
        Q_DECLARE_FLAGS( Constraints, Constraint )

        enum Anchors {
            NoAnchor          = 0,
            AnchorTop         = 1,
            AnchorBottom      = 2,
            AnchorLeft        = 3,
            AnchorRight       = 4,
            AnchorTopLeft     = 5,
            AnchorBottomLeft  = 6,
            AnchorTopRight    = 7,
            AnchorBottomRight = 8,
        };

        enum Gravity {
            NoGravity          = 0,
            GravityTop         = 1,
            GravityBottom      = 2,
            GravityLeft        = 3,
            GravityRight       = 4,
            GravityTopLeft     = 5,
            GravityBottomLeft  = 6,
            GravityTopRight    = 7,
            GravityBottomRight = 8,
        };

        XdgPositioner();
        XdgPositioner( const XdgPositioner *other );
        XdgPositioner( xdg_positioner *pos, const QSize& initialSize = QSize(), const QRect& anchor = QRect() );
        ~XdgPositioner();

        Anchors anchorEdge() const;
        void setAnchorEdge( Anchors edge );

        Gravity gravity() const;
        void setGravity( Gravity edge );

        QRect anchorRect() const;
        void setAnchorRect( const QRect& anchor );

        QSize initialSize() const;
        void setInitialSize( const QSize& size );

        Constraints constraints() const;
        void setConstraints( Constraints constraints );

        QPoint anchorOffset() const;
        void setAnchorOffset( const QPoint& offset );

        void setReactive();

        xdg_positioner *get();

    private:
        xdg_positioner *mObj;

        QSize mInitialSize;
        QRect mAnchorRect;
        Gravity mGravity;
        Anchors mAnchorEdge;
        Constraints mConstraints;
        QPoint mAnchorOffset;
};

Q_DECLARE_OPERATORS_FOR_FLAGS( WQt::XdgPositioner::Constraints );

Q_DECLARE_METATYPE( WQt::XdgPositioner );
Q_DECLARE_METATYPE( WQt::XdgPositioner::Constraint );
Q_DECLARE_METATYPE( WQt::XdgPositioner::Constraints );
