/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 * Copyright (c) 2021 Abrar (https://gitlab.com/s96Abrar)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/

#pragma once

#include <QObject>
#include <QSize>
#include <QRect>

struct wl_seat;

struct xdg_surface;
struct xdg_popup;
struct xdg_surface_listener;
struct xdg_popup_listener;

namespace WQt {
    class XdgPopup;
}

class WQt::XdgPopup : public QObject {
    Q_OBJECT;

    public:
        XdgPopup( xdg_surface *surface, xdg_popup *popup );
        ~XdgPopup();

        void grab( wl_seat *seat, quint32 serial );

        void ackConfigure( quint32 serial );

        xdg_popup *get();
        xdg_surface *xdgSurface();

    private:
        static void handleConfigure( void *data, xdg_popup *popup, int32_t x, int32_t y, int32_t w, int32_t h );
        static void handlePopupDone( void *data, xdg_popup *xdg_popup );
        static void handlePopupRepositioned( void *data, xdg_popup *xdg_popup, uint32_t serial );
        static void handleSurfaceConfigure( void *data, xdg_surface *xdg_surface, uint32_t serial );

        xdg_popup *mObj;
        xdg_surface *mSurfObj;

        static const xdg_popup_listener mListener;
        static const xdg_surface_listener mSurfListener;

        QRect pendingRect;

    Q_SIGNALS:
        void popupDone();
        void popupRepositioned();
        void configureRequested( const QRect& relPos, quint32 serial );
};
